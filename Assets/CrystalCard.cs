using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalCard : MonoBehaviour
{
    public enum CrystalType
    {
        FIRE,
        ICE,
        LIGHTNING
    }

    public List<CrystalType> TypeOrder = new List<CrystalType>();
    private int ActiveCrystalType = 0;

    public void ResetCard()
    {
        PutInPlay();
    }

    public CrystalType GetActiveCrystalType()
    {
        return TypeOrder[ActiveCrystalType];
    }

    public void RotateClockwise()
    {
        if (ActiveCrystalType >= TypeOrder.Count - 1)
        {
            ActiveCrystalType = 0;
        }
        else
        {
            ActiveCrystalType++;
        }

        gameObject.transform.Rotate(0, 0, -90);
        PlayLogic.Instance.CheckPlayability();
        Debug.Log(GetActiveCrystalType());
    }

    public void RotateCounterClockwise()
    {
        if (ActiveCrystalType <= 0)
        {
            ActiveCrystalType = TypeOrder.Count - 1;
        }
        else
        {
            ActiveCrystalType--;
        }

        gameObject.transform.Rotate(0, 0, 90);
        PlayLogic.Instance.CheckPlayability();
        Debug.Log(GetActiveCrystalType());
    }

    public void PutInPlay()
    {
        CardEvents cardEvents = gameObject.GetComponent<CardEvents>();
        GameObject cardHit;
        GameObject panelHit;
        GameObject slotHit;
        cardEvents.GetPositionOnBoard(gameObject, out cardHit, out panelHit, out slotHit);
        if (panelHit.name == "CrystalPanel")
        {
            int slotIndex;
            if (int.TryParse(slotHit.name.Split('_')[1], out slotIndex))
            {
                PlayLogic.Instance.AddCrystalCard(gameObject, slotIndex);
            }
            else
            {
                Debug.LogError("CrystalCard.PutInPlay: Name parse error. This should never happen, check this logic.");
            }
        }
    }
}
