using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Deck : MonoBehaviour
{
    public List<GameObject> NonStarterCards = new List<GameObject>();
    public List<GameObject> BottomOfDeckCards = new List<GameObject>();
    public List<GameObject> DeckSlots = new List<GameObject>();

    public void Start()
    {
        PlayLogic.Instance.ResetGameEvent += ResetDeck;
    }

    public void OnDestroy()
    {
        PlayLogic.Instance.ResetGameEvent -= ResetDeck;
    }

    private void ResetDeck()
    {
        NonStarterCards.ForEach(x => { x.transform.SetParent(null); x.transform.position = PlayLogic.Instance.OffScreenPostion; });
        BottomOfDeckCards.ForEach(x =>{ x.transform.SetParent(null); x.transform.position = PlayLogic.Instance.OffScreenPostion; });

        System.Random random = new System.Random();
        NonStarterCards = NonStarterCards.OrderBy(x => random.Next()).ToList<GameObject>();

        foreach(GameObject card in BottomOfDeckCards)
        {
            card.transform.SetParent(PlayLogic.Instance.DeckGameObject.transform);
        }

        foreach(GameObject card in NonStarterCards)
        {
            card.transform.SetParent(PlayLogic.Instance.DeckGameObject.transform);
        }

        NonStarterCards[0].transform.position = DeckSlots[0].transform.position;
        NonStarterCards[0].transform.SetParent(PlayLogic.Instance.CardsInPlay.transform);
        NonStarterCards[0].GetComponent<ActionCard>().PutInPlay();

        NonStarterCards[1].transform.position = DeckSlots[1].transform.position;
        NonStarterCards[1].transform.SetParent(PlayLogic.Instance.CardsInPlay.transform);
        NonStarterCards[1].GetComponent<ActionCard>().PutInPlay();

        NonStarterCards[2].transform.position = DeckSlots[2].transform.position;
        NonStarterCards[2].transform.SetParent(PlayLogic.Instance.CardsInPlay.transform);
        NonStarterCards[2].GetComponent<ActionCard>().PutInPlay();
    }
}
