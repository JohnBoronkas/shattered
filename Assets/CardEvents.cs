using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardEvents : EventTrigger
{
    private Vector3 DraggedCardInitialPosition;
    private GameObject DraggedCardInitialPanel;

    public override void OnBeginDrag(PointerEventData data)
    {
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(data, raycastResults);
        foreach(RaycastResult result in raycastResults)
        {
            if (result.gameObject.tag == "Panel")
            {
                DraggedCardInitialPanel = result.gameObject;
            }
        }

        GameObject draggedCard = data.pointerPress;
        draggedCard.transform.SetParent(PlayLogic.Instance.Focus.transform);
        DraggedCardInitialPosition = draggedCard.transform.position;

        Debug.Log($"Picked up card {(draggedCard ? draggedCard.name : "none")} from panel {(DraggedCardInitialPanel ? DraggedCardInitialPanel.name : "none")}.");
    }

    public override void OnDrag(PointerEventData data)
    {
        data.pointerPress.transform.position += (Vector3)data.delta;
    }

    public override void OnEndDrag(PointerEventData data)
    {
        GameObject draggedCard = data.lastPress;
        data.position = draggedCard.transform.position;

        GameObject cardHit = null;
        GameObject panelHit = null;
        GameObject slotHit = null;
        GetPositionOnBoard(draggedCard, out cardHit, out panelHit, out slotHit);
        DoEndDragActions(draggedCard, DraggedCardInitialPanel, cardHit, panelHit, slotHit);
    
        draggedCard.transform.SetParent(PlayLogic.Instance.CardsInPlay.transform);
        DraggedCardInitialPosition = Vector3.zero;
    }

    public void GetPositionOnBoard(GameObject draggedCard, out GameObject cardHit, out GameObject panelHit, out GameObject slotHit)
    {
        cardHit = null;
        panelHit = null;
        slotHit = null;

        PointerEventData pointerData = new PointerEventData(EventSystem.current);
        pointerData.position = draggedCard.transform.position;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, raycastResults);
        foreach(RaycastResult result in raycastResults)
        {
            GameObject hitObject = result.gameObject;

            if (hitObject.tag == "Card" && hitObject != draggedCard)
            {
                cardHit = hitObject;
            }

            if (hitObject.tag == "Panel")
            {
                panelHit = hitObject;
            }

            if (hitObject.tag == "Slot")
            {
                slotHit = hitObject;
            }
        }
    }

    private void DoEndDragActions(GameObject draggedCard, GameObject draggedCardInitialPanel, GameObject cardHit, GameObject panelHit, GameObject slotHit)
    {
        Debug.Log($"Dropped card {(draggedCard ? draggedCard.name : "none")} onto panel {(panelHit ? panelHit.name : "none")}, slot {(slotHit ? slotHit.name : "none")}, card {(cardHit ? cardHit.name : "none")}.");

        void ResetMove()
        {
            // Reset the dragged card
            draggedCard.transform.position = DraggedCardInitialPosition;
        }

        if (draggedCardInitialPanel == null || panelHit == null)
        {
            // Invalid move
            ResetMove();
            Debug.Log($"Invalid move! {(draggedCardInitialPanel ? "" : "draggedCardInitialPanel is null!")} {(panelHit ? "" : "panelHit is null!")}");
        }
        else if(slotHit && cardHit && draggedCardInitialPanel.name == "HandPanel" && panelHit.name == "HandPanel")
        {
            // Do hand-to-hand swap
            draggedCard.transform.position = cardHit.transform.position;
            cardHit.transform.position = DraggedCardInitialPosition;
            draggedCard.GetComponent<ActionCard>().PutInPlay();
            cardHit.GetComponent<ActionCard>().PutInPlay();
            Debug.Log($"Did hand-to-hand swap of {draggedCard.name} with {cardHit.name}.");
        }
        else if(PlayLogic.Instance.CurrentMode == PlayLogic.Mode.SETUP && slotHit && cardHit && draggedCardInitialPanel.name == "CrystalPanel" && panelHit.name == "CrystalPanel")
        {
            // Do crystal-to-crystal swap during setup
            draggedCard.transform.position = cardHit.transform.position;
            cardHit.transform.position = DraggedCardInitialPosition;
            draggedCard.GetComponent<CrystalCard>().PutInPlay();
            cardHit.GetComponent<CrystalCard>().PutInPlay();
            Debug.Log($"Did crystal-to-crystal swap of {draggedCard.name} with {cardHit.name} during setup.");
        }
        else if(slotHit && !cardHit && (draggedCardInitialPanel.name == "HandPanel" || draggedCardInitialPanel.name == "DeckPanel") && panelHit.name == "HandPanel")
        {
            // Move card from hand or deck to empty slot in hand
            draggedCard.transform.position = slotHit.transform.position;
            draggedCard.GetComponent<ActionCard>().PutInPlay();

            if (draggedCardInitialPanel.name == "DeckPanel")
            {
                // Moved card came from deck, so refil the empty deck slot if there are cards left in the deck
                int numCardsLeftInDeck = PlayLogic.Instance.DeckGameObject.transform.childCount;
                if (numCardsLeftInDeck > 0) {
                    Transform topDeckCard = PlayLogic.Instance.DeckGameObject.transform.GetChild(numCardsLeftInDeck - 1);
                    topDeckCard.position = DraggedCardInitialPosition;
                    topDeckCard.SetParent(PlayLogic.Instance.CardsInPlay.transform);
                    topDeckCard.GetComponent<ActionCard>().PutInPlay();

                    Debug.Log($"Moved deck slot card {draggedCard.name} to empty hand slot. Repenished with card {topDeckCard.name} from deck with {numCardsLeftInDeck} cards left in the deck.");
                }
                else
                {
                    Debug.Log("Deck out of cards, not replenishing deck slot.");
                }
            }
            else
            {
                Debug.Log($"Moved in-hand card {draggedCard.name} to empty hand slot.");
            }
        }
        else
        {
            // Invalid move
            ResetMove();
            Debug.Log("Invalid move! Fallthrough case!");
        }

        PlayLogic.Instance.CheckPlayability();
    }
}
