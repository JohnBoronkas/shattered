using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.UI;

public class ActionCard : MonoBehaviour
{
    private GameObject ActivateButton;
    public List<CrystalCard.CrystalType> CrystalCost = new List<CrystalCard.CrystalType>();

    public void Awake()
    {
        ActivateButton = Instantiate(GameObject.Find("ActivateCardButton"), new Vector3(transform.position.x, transform.position.y + 165, transform.position.z), Quaternion.identity);
        ActivateButton.transform.SetParent(gameObject.transform);
        ActivateButton.GetComponent<Button>().onClick.AddListener(PlayCard);
        ActivateButton.SetActive(false);
    }

    public void ResetCard()
    {
        PutInPlay();
    }

    public void PutInPlay()
    {
        CardEvents cardEvents = gameObject.GetComponent<CardEvents>();
        GameObject cardHit;
        GameObject panelHit;
        GameObject slotHit;
        cardEvents.GetPositionOnBoard(gameObject, out cardHit, out panelHit, out slotHit);
        if (panelHit.name == "HandPanel" || panelHit.name == "DeckPanel")
        {
            int slotIndex;
            if (int.TryParse(slotHit.name.Split('_')[1], out slotIndex))
            {
                if (panelHit.name == "HandPanel")
                {
                    PlayLogic.Instance.AddActionCardToHand(gameObject, slotIndex);
                }
                else if (panelHit.name == "DeckPanel")
                {
                    PlayLogic.Instance.AddActionCardToDeck(gameObject, slotIndex);
                }
            }
            else
            {
                Debug.LogError("ActionCard.PutInPlay: Name parse error. This should never happen, check this logic.");
            }
        }
    }

    public void CheckPlayability()
    {
        Debug.Log($"Checking Playability for {gameObject.name}");

        bool isPlayable = false;
        ReadOnlyCollection<CrystalCard.CrystalType> activeCrystalTypes = PlayLogic.Instance.GetActiveCrystalTypes();

        if (CrystalCost.Count <= 0)
        {
            // no cost, free to play
            isPlayable = true;
        }
        else if (activeCrystalTypes.Count < CrystalCost.Count)
        {
            // not enough crystals to play
            return;
        }
        else
        {
            // fuck. TODO do this better or something i guess

            for (int act = 0, cc = 0; act < activeCrystalTypes.Count; ++act)
            {
                if (CrystalCost[cc] == activeCrystalTypes[act])
                {
                    if (CrystalCost.Count == 1)
                    {
                        // Single resource cost, so we're good
                        isPlayable = true;
                        break;
                    }

                    // Check forward
                    int resourceCount = 1;
                    for (int cc2 = cc+1, act2 = act+1; cc2 < CrystalCost.Count; ++cc2)
                    {
                        if (act2 < activeCrystalTypes.Count && CrystalCost[cc2] == activeCrystalTypes[act2])
                        {
                            ++resourceCount;
                            ++act2;

                            if (resourceCount >= CrystalCost.Count)
                            {
                                isPlayable = true;
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    // Check backwards
                    resourceCount = 1;
                    for (int cc2 = cc+1, act2 = act-1; cc2 < CrystalCost.Count; ++cc2)
                    {
                        if (act2 >= 0 && CrystalCost[cc2] == activeCrystalTypes[act2])
                        {
                            ++resourceCount;
                            --act2;

                            if (resourceCount >= CrystalCost.Count)
                            {
                                isPlayable = true;
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (isPlayable)
                    {
                        break;
                    }
                }
            }
        }

        // TODO PICKUP Only is playable if card is in hand (can currently play cards from deck slots)
        if (isPlayable)
        {
            gameObject.GetComponent<Image>().color = Color.cyan;
            ActivateButton.SetActive(true);
        }
        else
        {
            gameObject.GetComponent<Image>().color = Color.white;
            ActivateButton.SetActive(false);
        }
    }

    public void PlayCard()
    {
        Debug.Log($"PlayCard for {gameObject.name}");
    }
}
