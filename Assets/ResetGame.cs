using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetGame : MonoBehaviour
{
    public List<GameObject> StartingHand = new List<GameObject>();
    public List<GameObject> StartingCrystals = new List<GameObject>();
    public List<GameObject> StartingBonusCrystals = new List<GameObject>();

    public List<GameObject> HandSlots = new List<GameObject>();
    public List<GameObject> BonusSlots = new List<GameObject>();
    public List<GameObject> CrystalSlots = new List<GameObject>();

    public void Start()
    {
        PlayLogic.Instance.ResetGameEvent += ResetBoard;
    }

    public void OnDestroy()
    {
        PlayLogic.Instance.ResetGameEvent -= ResetBoard;
    }

    private void ResetBoard()
    {
        SetStarting(StartingHand, HandSlots);
        StartingHand.ForEach(x => x.GetComponent<ActionCard>().ResetCard());

        SetStarting(StartingCrystals, CrystalSlots, 1);
        StartingCrystals.ForEach(x => x.GetComponent<CrystalCard>().ResetCard());

        SetStarting(StartingBonusCrystals, BonusSlots);

        Debug.Log("Game reset!");
    }

    private void SetStarting(List<GameObject> cards, List<GameObject> slots, int slotOffset=0)
    {
        for(int i = 0; i < cards.Count; i++)
        {
            cards[i].transform.position = slots[i+slotOffset].transform.position;
        }
    }
}
