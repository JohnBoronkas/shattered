using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

// TODO Fix gitignore
// TODO Play a card logic

public class PlayLogic : MonoBehaviour
{
    public static PlayLogic Instance { get; private set; }

    private Deck Deck;
    public GameObject DeckGameObject {
        get {
            return Deck.gameObject;
        }

        private set {
        }
    }

    private List<CrystalCard> CrystalsInPlay;

    private List<ActionCard> CardsInHand;

    private List<ActionCard> CardsInDeck;

    public GameObject CardsInPlay;

    public GameObject Focus;

    public Vector2 OffScreenPostion;

    public enum Mode
    {
        UNDEFINED,
        SETUP,
        PLAYING
    }
    public Mode CurrentMode { get; private set; }

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Deck = gameObject.GetComponent<Deck>();

            GameObject CrystalPanel = GameObject.Find("CrystalPanel");
            CrystalsInPlay = new List<CrystalCard>(CrystalPanel.transform.childCount);
            for (int i = 0; i < CrystalPanel.transform.childCount; i++)
            {
                CrystalsInPlay.Add(null);
            }

            GameObject HandPanel = GameObject.Find("HandPanel");
            CardsInHand = new List<ActionCard>(HandPanel.transform.childCount);
            for (int i = 0; i < HandPanel.transform.childCount; i++)
            {
                CardsInHand.Add(null);
            }

            GameObject DeckPanel = GameObject.Find("DeckPanel");
            CardsInDeck = new List<ActionCard>(DeckPanel.transform.childCount);
            for (int i = 0; i < DeckPanel.transform.childCount; i++)
            {
                CardsInDeck.Add(null);
            }
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public delegate void ResetGameAction();
    public event ResetGameAction ResetGameEvent;
    public void ResetGame()
    {
        CurrentMode = Mode.SETUP;
        ResetGameEvent?.Invoke();
        CheckPlayability();
    }

    public void StartGame()
    {
        CurrentMode = Mode.PLAYING;
        CheckPlayability();
    }

    public void AddCrystalCard(GameObject crystalCard, int slotIndex)
    {
        CrystalsInPlay[slotIndex] = crystalCard.GetComponent<CrystalCard>();
        string log = "Crystals in play: ";
        CrystalsInPlay.ForEach(x => log += (x ? x.GetActiveCrystalType().ToString() : "none") + " ");
        Debug.Log(log);
    }

    public void AddActionCardToHand(GameObject actionCard, int slotIndex)
    {
        CardsInHand[slotIndex] = actionCard.GetComponent<ActionCard>();
        string log = "Cards in hand: ";
        CardsInHand.ForEach(x => log += (x ? x.name : "none") + " ");
        Debug.Log(log);
    }

    public void AddActionCardToDeck(GameObject actionCard, int slotIndex)
    {
        CardsInDeck[slotIndex] = actionCard.GetComponent<ActionCard>();
        string log = "Cards in Deck: ";
        CardsInDeck.ForEach(x => log += (x ? x.name : "none") + " ");
        Debug.Log(log);
    }

    public ReadOnlyCollection<CrystalCard.CrystalType> GetActiveCrystalTypes()
    {
        List<CrystalCard.CrystalType> activeCrystalTypes = new List<CrystalCard.CrystalType>();
        foreach (CrystalCard crystalCard in CrystalsInPlay) {
            if (crystalCard == null)
            {
                continue;
            }

            activeCrystalTypes.Add(crystalCard.GetActiveCrystalType());
        }
        return activeCrystalTypes.AsReadOnly();
    }

    public void CheckPlayability()
    {
        CardsInHand.ForEach(x => x?.CheckPlayability());
        CardsInDeck.ForEach(x => x?.CheckPlayability());
    }
}
